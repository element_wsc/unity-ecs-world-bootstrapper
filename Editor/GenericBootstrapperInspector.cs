﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Element.ECS.Bootstrapper.Editor
{
	[CustomEditor( typeof( GenericBootstrapper ) )]
	public class GenericBootstrapperInspector : UnityEditor.Editor
	{
		private List<Type> _allTypes;
		private List<string> _displayNames;
		private HashSet<Type> _selectedTypes = new HashSet<Type>();
		private GenericBootstrapper _bootstrapper;
		private SerializedProperty _worldName;
		private SerializedProperty _setAsActiveWorld;
		private SerializedProperty _useSceneNameAsWorldName;

		private void OnEnable()
		{
			_bootstrapper = (GenericBootstrapper)target;
			FindTypes();
			var typeNames = _bootstrapper.typeNames;
			if (typeNames != null)
			{
				foreach (var name in typeNames)
				{
					var type = Type.GetType( name );
					_selectedTypes.Add( type );
					_displayNames.Remove( type.Name );
				}
			}
			_displayNames.Sort();

			_worldName = serializedObject.FindProperty( "_worldName" );
			_setAsActiveWorld = serializedObject.FindProperty( "_setAsActiveWorld" );
			_useSceneNameAsWorldName = serializedObject.FindProperty( "_useSceneNameAsWorldName" );
		}

		public override void OnInspectorGUI()
		{
			serializedObject.UpdateIfRequiredOrScript();
			EditorGUILayout.PropertyField( _useSceneNameAsWorldName, new GUIContent( "Use Scene name", "Uses the Scene name as world name" ) );

			if (_useSceneNameAsWorldName.boolValue == false)
				EditorGUILayout.PropertyField( _worldName );

			EditorGUILayout.PropertyField( _setAsActiveWorld );

			GUILayout.Space( 15 );

			var index = EditorGUILayout.Popup( "Add system", -1, _displayNames.ToArray() );

			if (index >= 0)
			{

				int i = _allTypes.FindIndex( (type) => type.Name == _displayNames[ index ] );
				_selectedTypes.Add( _allTypes[ i ] );
				_displayNames.Remove( _allTypes[ i ].Name );
				_displayNames.Sort();
				UpdateTarget();
			}

			Type typeToRemove = null;
			EditorGUI.indentLevel++;
			foreach (var item in _selectedTypes)
			{
				EditorGUILayout.BeginHorizontal();

				EditorGUILayout.LabelField( item.Name );
				if (GUILayout.Button( "X" ))
					typeToRemove = item;

				EditorGUILayout.EndHorizontal();
			}
			EditorGUI.indentLevel--;
			if (typeToRemove != null)
			{
				_selectedTypes.Remove( typeToRemove );
				_displayNames.Add( typeToRemove.Name );
				_displayNames.Sort();
				UpdateTarget();
			}
			serializedObject.ApplyModifiedProperties();
		}

		private void UpdateTarget()
		{
			var names = new List<string>( _selectedTypes.Count );
			foreach (var type in _selectedTypes)
				names.Add( type.AssemblyQualifiedName );

			_bootstrapper.typeNames = names.ToArray();
			EditorUtility.SetDirty( target );
		}

		private void UpdateDisplayNames()
		{
			//var count = _allTypes.Count - _selectedTypes.Count;
			//_displayNames = new string[ count ];
		}

		private void FindTypes()
		{
			_allTypes = new List<Type>( _bootstrapper.GetAllTypes() );

			_displayNames = new List<string>();
			for (int i = 0; i < _allTypes.Count; i++)
				_displayNames.Add( _allTypes[ i ].Name );
		}

	}
}



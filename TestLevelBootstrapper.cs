﻿using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Transforms2D;

namespace Element.ECS.Bootstrapper
{
	public class TestLevelBootstrapper : BootstrapperBase
	{
		protected override World OnCreateWorld()
		{
			return new World( "Test World" );
		}

		protected override void OnRegisterSystems()
		{
			// Add your Systems
			// RegisterSystem<CustomSystem>();




			// Unity Systems
			RegisterSystem<HeadingSystem>();
			RegisterSystem<MoveForwardSystem>();
			RegisterSystem<TransformInputBarrier>();
			RegisterSystem<TransformSystem>();
			RegisterSystem<MeshInstanceRendererSystem>();
			RegisterSystem<MoveForward2DSystem>();
			RegisterSystem<Transform2DSystem>();
			RegisterSystem<CopyInitialTransformFromGameObjectSystem>();
			RegisterSystem<CopyTransformFromGameObjectSystem>();
			RegisterSystem<CopyTransformToGameObjectSystem>();
		}
	}
}


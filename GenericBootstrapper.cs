﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Entities;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Element.ECS.Bootstrapper
{
	public class GenericBootstrapper : BootstrapperBase
	{
		[SerializeField] private string _worldName = "World";
		[SerializeField] private bool _setAsActiveWorld = true;
		[SerializeField] private bool _useSceneNameAsWorldName = false;
		public string[] typeNames;

		protected override World OnCreateWorld()
		{
			SetWorldActive = _setAsActiveWorld;

			var name = _useSceneNameAsWorldName ? SceneManager.GetActiveScene().name : _worldName;

			return new World( name );
		}

		protected override void OnRegisterSystems()
		{
			foreach (var name in typeNames)
			{
				var type = Type.GetType( name );
				if (type == null)
				{
					Debug.LogWarning( $"The Type {type.FullName} is missing" );
					continue;
				}
				RegisterSystem( type );
			}
		}

		public Type[] GetAllTypes()
		{
			var allTypes = new List<Type>();
			foreach (var ass in AppDomain.CurrentDomain.GetAssemblies())
			{
				var types = ass.GetTypes();

				// Create all ComponentSystem
				var systemTypes = types.Where( t =>
					 t.IsSubclassOf( typeof( ComponentSystemBase ) ) &&
					 t != typeof( EndFrameBarrier ) &&
					 !t.IsAbstract &&
					 !t.ContainsGenericParameters &&
					 t.GetCustomAttributes( typeof( DisableAutoCreationAttribute ), true ).Length == 0 );
				allTypes.AddRange( systemTypes );
			}
			allTypes.Sort( (l, r) => l.AssemblyQualifiedName.CompareTo( r.AssemblyQualifiedName ) );
			return allTypes.ToArray();
		}


	}
}

